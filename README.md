

# Docker commands

### mongodb container
docker run -P -d --name mongodb mongo
docker exec -it mongodb sh

java -Dspring.data.mongodb.uri=mongodb://192.168.99.100:32777/micros -jar build/libs/Employee.jar

### build image
docker build -t microservicedemo/employee .
### start container
docker run -P -d --name employee --link mongodb microservicedemo/employee

### --link
 * Creates a hosts file entry in the employee container pointing at the running location of MongoDB container
 * Inserts a number of environment variables in the employee container to assist with a programmatic access

 [pado@iMac spring-docker] $ docker exec employee bash -c 'env | grep MONGODB'
 MONGODB_NAME=/employee/mongodb
 MONGODB_ENV_GPG_KEYS=DFFA3DCF326E302C4787673A01C4E7FAAAB2461C 	42F3E95A2C4F08279C4960ADD68FA50FEA312927
 MONGODB_PORT_27017_TCP=tcp://172.17.0.2:27017
 MONGODB_PORT=tcp://172.17.0.2:27017
 MONGODB_ENV_MONGO_VERSION=3.2.10
 MONGODB_ENV_GOSU_VERSION=1.7
 MONGODB_PORT_27017_TCP_PORT=27017
 MONGODB_ENV_MONGO_MAJOR=3.2
 MONGODB_PORT_27017_TCP_PROTO=tcp
 MONGODB_PORT_27017_TCP_ADDR=172.17.0.2

 * Allows containers to communicate directly over ports exposed


### docker compose

docker-compose up -d
docker-compose ps
docker-compose scale employee=3

### HA Proxy
https://github.com/docker/dockercloud-haproxy

### Futher research
Building out a front-end in an container, or in a mobile app
Include batch processing of back-end data
Dynamic sizing of container cluster to process queue entires
Migrate a service from Java/Spring Boot to Scala/Akka/Play
Setting up CI
Building out my own image repository or using a container repository service (Google or Docker Hub)
Evaluating Container management systems like AWS’ ECS or Kubernates

