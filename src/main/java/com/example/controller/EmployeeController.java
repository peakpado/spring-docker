package com.example.controller;

import com.example.data.Employee;
import com.example.data.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by pado on 10/15/16.
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Employee create(@RequestBody Employee employee) {
        Employee result = employeeRepository.save(employee);
        return result;
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public Employee get(@PathVariable String employeeId) {
        return employeeRepository.findOne(employeeId);
    }

}


