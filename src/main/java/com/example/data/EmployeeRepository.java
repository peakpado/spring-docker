package com.example.data;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by pado on 10/15/16.
 */
public interface EmployeeRepository extends MongoRepository<Employee, String> {
}
